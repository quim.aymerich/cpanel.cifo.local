var xhr;
var tabla;



if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE
	xhr = new ActiveXObject("Microsoft.XMLHTTP");
}

window.onload=function(){
	tabla=$('#dataTableRoles').DataTable({
		"language": {
	        url: '/vendor/datatables/es_ES.json'
	    },
	    "pageLength"	: 5 ,
	    "lengthMenu"	: [ 5, 10, 25, 50, 75, 100 ],
	    "stateSave" 	: true,
	    "columnDefs" 	: [
	    	{"data":"id"		, "targets":0, "searchable": true,  "orderable": true },
	    	{"data":"nombre"	, "targets":1, "searchable": true,  "orderable": true },
	    	{"data":"nivel"		, "targets":2, "searchable": true,  "orderable": true },
	    	{"data":"edit"		, "targets":3, "searchable": false, "orderable": false , "className": "text-center"},
	    	{"data":"remove"	, "targets":4, "searchable": false, "orderable": false , "className": "text-center"}
	    ]
	});
	$("#modalAjax").modal("show");
	getRoles();
}

/* ----------------------------------- getRoles ------------------------------------------- */
function getRoles(){
	tabla.clear().draw(false);
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	for(i in objResponse.records){
		    		tabla.row.add(  {
			    		"id" 		: objResponse.records[i].id,
			    		"nombre" 	: objResponse.records[i].nombre,
			    		"nivel" 	: objResponse.records[i].nivel,
			    		"edit" 		: '<a class="text-info" href="javascript:editRol('+objResponse.records[i].id+')" aria-label="Editar"><i class="fa fa-edit"></i></a>',
			    		"remove" 	: '<a class="text-danger" href="javascript:removeRol('+objResponse.records[i].id+')" aria-label="Eliminar"><i class="fa fa-trash"></i></a>',
			    		});
		    	}
		    	tabla.draw(false);
		    }
		    $("#modalAjax").modal("hide");
		    var endTime= new Date();
		    document.querySelector("#cardRoles div.card-footer span").innerHTML= endTime-initTime;
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/roles/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/* ---------------------------------- removeRol ------------------------------------------*/
function removeRol(id){
	document.querySelector('#removeModal input').value=id;
	$("#removeModal").modal("show");
}

document.getElementById('btnRemove').onclick=function(){
	var id=document.querySelector('#removeModal input').value;
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			// tot va bé, s'ha rebut la resposta
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	$("#removeModal").modal("hide");
	        	getRoles();
		    }else{
		    	alert(objResponse.msg);
		    }
		}
	}
	xhr.open('DELETE','http://app.cifo.local/api/private/cifo/roles/'+id,true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
};

/* ----------------------------------- editRol --------------------------------------------*/
function editRol(id){
	$("#modalAjax").modal("show");
	document.querySelectorAll("#editModal form input[type=text]").forEach(function(item){
		item.value=''
	});
	
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			// tot va bé, s'ha rebut la resposta
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	var objForm= document.querySelector("#editModal form");
		    	for (key in objResponse.record ){
		    		if (typeof objForm[key] !== 'undefined') {
		    			objForm[key].value=objResponse.record[key];
		    		}
		    	}
		    	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
		    	objForm.querySelectorAll("*").forEach(function(item){
		    		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
		    	});
		    	$("#editModal").modal("show");
		    }
		    $("#modalAjax").modal("hide");
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/roles/'+id+'/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/* ----------------------- #btnSave onclick -----------------------------------------------------*/
document.getElementById("btnSave").onclick=function(evt){

	var objForm= document.querySelector("#editModal form");

	// var totOk control de enviament
	var totOk=true;
	
	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
	objForm.querySelectorAll("*").forEach(function(item){
		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
	});
	
	// ------------------------ nombre --------------------
	objForm["nombre"].value= objForm["nombre"].value.trim();
	var pattern = /\d/;
	if( pattern.test(objForm["nombre"].value) || objForm["nombre"].value==""){
		objForm["nombre"].className+=' is-invalid';
		totOk=false;
	}
	
	// ------------------------ nivel --------------------
	objForm["nivel"].value= objForm["nivel"].value.trim();
	var pattern = /^[0-9]*$/;
	if( !pattern.test(objForm["nivel"].value) || objForm["nivel"].value==""){
		objForm["nivel"].className+=' is-invalid';
		totOk=false;
	}
	
	
	if(totOk){
		
		xhr.onreadystatechange =function(){
	        if (xhr.readyState == 4 && xhr.status == 200) {
	          var response=JSON.parse(xhr.responseText);
	          $("#modalAjax").modal("hide");
	          if( response.status){
	        	  $("#editModal").modal("hide");
	        	  getRoles();
	          }else{
	        	  alert(response.msg);
	          }
	        }
		}
		
		var formData = new FormData(document.querySelector("#editModal form"));
	    var object = {};
	    formData.forEach(function(value, key){
	      	object[key] = value;  
	    });
      	xhr.open('PUT','http://app.cifo.local/api/private/cifo/roles/',true);
      	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
        xhr.send(JSON.stringify(object));
	}
}

