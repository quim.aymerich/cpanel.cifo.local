var xhr;

if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE
	xhr = new ActiveXObject("Microsoft.XMLHTTP");
}


/* -------------------------------------submit ---------------------------------*/
document.getElementById("btnSave").onclick=function(evt){

	evt.preventDefault();

	// var totOk control de enviament
	var totOk=true;
	
	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
	this.form.querySelectorAll("*").forEach(function(item){
		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
	});
	
	// ------------------------ nombre --------------------
	this.form.nombre.value= this.form.nombre.value.trim();
	var pattern = /\d/;
	if( pattern.test(this.form.nombre.value) || this.form.nombre.value==""){
		this.form.nombre.className+=' is-invalid';
		totOk=false;
	}
	
	// ------------------------ nivel --------------------
	this.form.nivel.value= this.form.nivel.value.trim();
	var pattern = /^[0-9]*$/;
	if( !pattern.test(this.form.nivel.value) || this.form.nivel.value==""){
		this.form.nivel.className+=' is-invalid';
		totOk=false;
	}
	
	if(totOk){
		xhr.onreadystatechange =function(){
	        if (xhr.readyState == 4 && xhr.status == 200) {
	          var response=JSON.parse(xhr.responseText);
	          if( response.status){
	        	  document.getElementById("btnReset").click();
	        	  $("#saveModal").modal("show");
	          }else{
	        	  alert(response.msg);
	          }
	        }
		}
		var formData = new FormData(this.form);
      	var object = {};
      	formData.forEach(function(value, key){
      			object[key] = value;  	
      	});
      	
      	xhr.open('POST','http://app.cifo.local/api/private/cifo/roles/',true);
      	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
        xhr.send(JSON.stringify(object));
	}
}

var endTime= new Date();
document.querySelector("#cardRoles div.card-footer span").innerHTML= endTime-initTime;