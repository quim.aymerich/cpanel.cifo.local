var xhr;
var tabla;



var glogal_id_provincias;
var global_id_municipios;

if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE
	xhr = new ActiveXObject("Microsoft.XMLHTTP");
}

window.onload=function(){
	tabla=$('#dataTableMunicipios').DataTable({
		"language": {
	        url: '/vendor/datatables/es_ES.json'
	    },
	    "pageLength"	: 5 ,
	    "lengthMenu"	: [ 5, 10, 25, 50, 75, 100 ],
	    "stateSave" 	: true,
	    "columnDefs" 	: [
	    	{"data":"id"		, "targets":0, "searchable": true,  "orderable": true },
	    	{"data":"nombre"	, "targets":1, "searchable": true,  "orderable": true },
	    	{"data":"provincia"	, "targets":2, "searchable": true,  "orderable": true },
	    	{"data":"comunidad"	, "targets":3, "searchable": true,  "orderable": true },
	    	{"data":"active"	, "targets":4, "searchable": true,  "orderable": true },
	    	{"data":"edit"		, "targets":5, "searchable": false, "orderable": false , "className": "text-center"},
	    	{"data":"remove"	, "targets":6, "searchable": false, "orderable": false , "className": "text-center"}
	    ]
	});
	$("#modalAjax").modal("show");
	getComunidades();
}

/*-------------------------------------------- getComunidades -----------------------------------------*/
function getComunidades(){
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	for(i in objResponse.records){
		    		var cadena=`<option value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
		    		document.getElementById("id_comunidades").innerHTML+=cadena;
		    		document.getElementById("searchComunidad").innerHTML+=cadena;
		    	}
		    }
		    getMunicipios();
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/comunidades/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/*-------------------------------------------- getProvincias -----------------------------------------*/
function getProvincias(id_comunidades, id_provincias=''){
	document.getElementById("id_provincias").innerHTML='<option value="">-- Elija una opción --</option>';
	if(id_comunidades!=''){
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 &&  xhr.status == 200 ) {
				var objResponse=JSON.parse(xhr.responseText);
			    if(objResponse.status==true){
			    	for(i in objResponse.records){
			    		if( objResponse.records[i].id==id_provincias ){
			    			var selected='selected="selected"';
			    		}else{
			    			var selected = '';
			    		}
			    		var cadena=`<option ${selected} value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
			    		document.getElementById("id_provincias").innerHTML+=cadena;
			    	}
			    }
			}
		}
		xhr.open('GET','http://app.cifo.local/api/private/cifo/provincias/comunidad/'+id_comunidades+'/',true);
		xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
		xhr.send(null);
	}
}

/* -------------------------------------- id_comunidades onchange ---------------------------------------*/
document.getElementById("id_comunidades").onchange=function(){
	getProvincias(this.value);
};

/* ---------------------------------------- getMunicipios ------------------------------------------------*/
function getMunicipios(){
	tabla.clear().draw(false);

	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	for(i in objResponse.records){
		    		if(objResponse.records[i].active=="1"){
		    			var circle='<span class="text-success"><i class="fas fa-check-circle"></i></span>';
		    		}else{
		    			var circle='<span class="text-danger"><i class="fas fa-times-circle"></i></span>';
		    		}
		    		
		    		
		    		tabla.row.add(  {
		    		"id" 		: objResponse.records[i].id,
		    		"nombre" 	: objResponse.records[i].nombre,
		    		"provincia" : objResponse.records[i].provincia,
		    		"comunidad" : objResponse.records[i].comunidad,
		    		"active" 	: circle,
		    		"edit" 		: '<a class="text-info" href="#" onclick="editMunicipio('+objResponse.records[i].id+')" aria-label="Editar"><i class="fa fa-edit"></i></a>',
		    		"remove" 	:'<a class="text-danger" href="#" onclick="removeMunicipio('+objResponse.records[i].id+')" aria-label="Eliminar"><i class="fa fa-trash"></i></a>',
		    		});
		    	}
		    	tabla.draw(false);
		    }else{
		    	alert(objResponse.msg);
		    }
		    $("#modalAjax").modal("hide");
		    var endTime= new Date();
		    document.querySelector("#cardMunicipios div.card-footer span").innerHTML= endTime-initTime;
		    
		}else if(xhr.readyState == 4 &&  xhr.status == 401){
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==false){
		    	alert(objResponse.msg);
		    	location.href="/login.html";
		    }
		}
	}
	var filter="";
	if(document.getElementById("searchProvincia").value!=""){
		// Definim els parametres de sol·licitus HTTP
		filter+="provincia/"+document.getElementById("searchProvincia").value+'/';
	}else 	if(document.getElementById("searchComunidad").value!=""){
		filter+="comunidad/"+document.getElementById("searchComunidad").value+'/';
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/municipios/'+filter,true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/* ----------------------------------- editMunicipio --------------------------------------------*/
function editMunicipio(id){
	
	$("#modalAjax").modal("show");
	
	document.querySelectorAll("#editModal form input[type=text]").forEach(function(item){
		item.value=''
	});
	document.querySelectorAll("#editModal form select").forEach(function(item){
		item.value=''
	});
	document.querySelectorAll("#editModal form input[type=radio]").forEach(function(item){
		item.checked=false;
	});
	
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			// tot va bé, s'ha rebut la resposta
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	var objForm= document.querySelector("#editModal form");
		    	for (key in objResponse.record ){
		    		if (typeof objForm[key] !== 'undefined') {
		    			objForm[key].value=objResponse.record[key];
		    		}
		    	}
		    	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
		    	objForm.querySelectorAll("*").forEach(function(item){
		    		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
		    	});
		    	
		    	getProvincias(objResponse.record.id_comunidades,objResponse.record.id_provincias);
		    	$("#editModal").modal("show");
		    }
		    $("#modalAjax").modal("hide");
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/municipios/'+id+'/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}


/* ----------------------- #btnSave onclick -----------------------------------------------------*/
document.getElementById("btnSave").onclick=function(evt){

	var objForm= document.querySelector("#editModal form");

	// var totOk control de enviament
	var totOk=true;
	
	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
	objForm.querySelectorAll("*").forEach(function(item){
		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
	});
	
	// ------------------------ nombre --------------------
	objForm["nombre"].value= objForm["nombre"].value.trim();
	var pattern = /\d/;
	if( pattern.test(objForm["nombre"].value) || objForm["nombre"].value==""){
		objForm["nombre"].className+=' is-invalid';
		totOk=false;
	}
	
	// ------------------------ id_provincias --------------------
	if( objForm["id_provincias"].value==""){
		objForm["id_provincias"].className+=' is-invalid';
		totOk=false;
	}
	
	// ------------------------ active --------------------
	if( objForm["active"].value==""){
		this.form.querySelectorAll('input[name=active]').forEach(function(item){
			  item.className+= " is-invalid ";
		 });
		totOk=false;
	}
	// ------------------------ poblacion --------------------
	objForm["poblacion"].value= objForm["poblacion"].value.trim();
	var pattern = /^[0-9]*$/;
	if( !pattern.test(objForm["poblacion"].value) || objForm["poblacion"].value==""){
		objForm["poblacion"].className+=' is-invalid';
		totOk=false;
	}
	// ------------------------ hombres --------------------
	objForm["hombres"].value= objForm["hombres"].value.trim();
	var pattern = /^[0-9]*$/;
	if( !pattern.test(objForm["hombres"].value) || objForm["hombres"].value==""){
		objForm["hombres"].className+=' is-invalid';
		totOk=false;
	}
	// ------------------------ mujeres --------------------
	objForm["mujeres"].value= objForm["mujeres"].value.trim();
	var pattern = /^[0-9]*$/;
	if( !pattern.test(objForm["mujeres"].value) || objForm["mujeres"].value==""){
		objForm["mujeres"].className+=' is-invalid';
		totOk=false;
	}
	
	if(totOk){
		xhr.onreadystatechange =function(){
	        if (xhr.readyState == 4 && xhr.status == 200) {
	          var objResponse=JSON.parse(xhr.responseText);
	          $("#modalAjax").modal("hide");
	          if( objResponse.status){
	        	  $("#editModal").modal("hide");
	        	  $("#modalAjax").modal("show");
	        	  getMunicipios();
	          }else{
	        	  alert(objResponse.msg);
	          }
	        }
		}
		var formData = new FormData(document.querySelector("#editModal form"));
	    var object = {};
	    formData.forEach(function(value, key){
	      	object[key] = value;  
	    });
      	xhr.open('PUT','http://app.cifo.local/api/private/cifo/municipios/',true);
      	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
        xhr.send(JSON.stringify(object));
	}
}

/* ---------------------------------- removeMunicipio ------------------------------------------*/
function removeMunicipio(id){
	document.querySelector('#removeModal input').value=id;
	$("#removeModal").modal("show");
}

document.getElementById('btnRemove').onclick=function(){
	var id=document.querySelector('#removeModal input').value;
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			// tot va bé, s'ha rebut la resposta
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	$("#removeModal").modal("hide");
		    	$("#modalAjax").modal("show");
		    	getMunicipios();
		    }else{
		    	alert(objResponse.msg);
		    }
		}
	}
	xhr.open('DELETE','http://app.cifo.local/api/private/cifo/municipios/'+id,true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
};



/* --------------------------- #searchComunidad onchange -------------------------------------------- */
document.getElementById("searchProvincia").onchange=function(){
	initTime= new Date();
	$("#modalAjax").modal("show");
	getMunicipios();
};

/* --------------------------- #searchComunidad onchange -------------------------------------------- */
document.getElementById("searchComunidad").onchange=function(){
	initTime= new Date();
	$("#modalAjax").modal("show");
	searchProvincias(this.value);
};

/*-------------------------------------------- getProvincias -----------------------------------------*/
function searchProvincias(id_comunidades){
	document.getElementById("searchProvincia").innerHTML='<option value="">-- Elija una opción --</option>';
	if(id_comunidades!=''){
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 &&  xhr.status == 200 ) {
				var objResponse=JSON.parse(xhr.responseText);
			    if(objResponse.status==true){
			    	for(i in objResponse.records){
			    		var cadena=`<option value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
			    		
			    		document.getElementById("searchProvincia").innerHTML+=cadena;
			    	}
			    }
			    getMunicipios();
			}
		}
		xhr.open('GET','http://app.cifo.local/api/private/cifo/provincias/comunidad/'+id_comunidades+'/',true);
		xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
		xhr.send(null);
	}else{
		getMunicipios();
	}
	
}