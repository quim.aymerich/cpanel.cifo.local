var xhr;

if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE
	xhr = new ActiveXObject("Microsoft.XMLHTTP");
}

window.onload=function(){
	$("#modalAjax").modal("show");
	getComunidades();
}

/*-------------------------------------------- getComunidades -----------------------------------------*/
function getComunidades(){
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	for(i in objResponse.records){
		    		var cadena=`<option value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
		    		document.getElementById("id_comunidades").innerHTML+=cadena;
		    	}
		    }
		    $("#modalAjax").modal("hide");
		    var endTime= new Date();
		    document.querySelector("#cardMunicipios div.card-footer span").innerHTML= endTime-initTime;
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/comunidades/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/* -------------------------------------- id_comunidades onchange ---------------------------------------*/
document.getElementById("id_comunidades").onchange=function(){
	getProvincias(this.value);
};


/*-------------------------------------------- getProvincias -----------------------------------------*/
function getProvincias(id_comunidades, id_provincias=''){
	document.getElementById("id_provincias").innerHTML='<option value="">-- Elija una opción --</option>';
	if(id_comunidades!=''){
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 &&  xhr.status == 200 ) {
				var objResponse=JSON.parse(xhr.responseText);
			    if(objResponse.status==true){
			    	for(i in objResponse.records){
			    		if( objResponse.records[i].id==id_provincias ){
			    			var selected='selected="selected"';
			    		}else{
			    			var selected = '';
			    		}
			    		var cadena=`<option ${selected} value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
			    		document.getElementById("id_provincias").innerHTML+=cadena;
			    	}
			    }
			}
		}
		xhr.open('GET','http://app.cifo.local/api/private/cifo/provincias/comunidad/'+id_comunidades+'/',true);
		xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
		xhr.send(null);
	}
}

/* -------------------------------------submit ---------------------------------*/
document.getElementById("btnSave").onclick=function(evt){

	evt.preventDefault();

	// var totOk control de enviament
	var totOk=true;
	
	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
	this.form.querySelectorAll("*").forEach(function(item){
		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
	});
	
	// ------------------------ nombre --------------------
	this.form.nombre.value= this.form.nombre.value.trim();
	var pattern = /\d/;
	if( pattern.test(this.form.nombre.value) || this.form.nombre.value==""){
		this.form.nombre.className+=' is-invalid';
		totOk=false;
	}
	// --------------------------- id_provincias -----------------------------------
	if(this.form.id_provincias.value==""){
		  this.form.id_provincias.className+=' is-invalid';
		  totOk=false;
	};
	// --------------------------- active -----------------------------------
	if(this.form.active.value==""){
		  this.form.querySelectorAll('input[name=active]').forEach(function(item){
			  item.className+= " is-invalid ";
			  
		  });
		  totOk=false;
	};
	// ------------------------ poblacion --------------------
	this.form.poblacion.value= this.form.poblacion.value.trim();
	var pattern = /^[0-9]*$/;
	if( !pattern.test(this.form.poblacion.value) || this.form.poblacion.value==""){
		this.form.poblacion.className+=' is-invalid';
		totOk=false;
	}
	// ------------------------ hombres --------------------
	this.form.hombres.value= this.form.hombres.value.trim();
	var pattern = /^[0-9]*$/;
	if( !pattern.test(this.form.hombres.value) || this.form.hombres.value==""){
		this.form.hombres.className+=' is-invalid';
		totOk=false;
	}
	// ------------------------ mujeres --------------------
	this.form.mujeres.value= this.form.mujeres.value.trim();
	var pattern = /^[0-9]*$/;
	if( !pattern.test(this.form.mujeres.value) || this.form.mujeres.value==""){
		this.form.mujeres.className+=' is-invalid';
		totOk=false;
	}
	
	if(totOk){
		$("#modalAjax").modal("show");
		xhr.onreadystatechange =function(){
	        if (xhr.readyState == 4 && xhr.status == 200) {
	          $("#modalAjax").modal("hide");
	          var response=JSON.parse(xhr.responseText);
	          if( response.status){
	        	  document.getElementById("btnReset").click();
	        	  $("#saveModal").modal("show");
	          }else{
	        	  alert(response.msg);
	          }
	        }
		}
		var formData = new FormData(this.form);
      	var object = {};
      	formData.forEach(function(value, key){
      			object[key] = value;  	
      	});
      	
      	xhr.open('POST','http://app.cifo.local/api/private/cifo/municipios/',true);
      	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
        xhr.send(JSON.stringify(object));
	}
}