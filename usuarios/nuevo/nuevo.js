
var imgSrc="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAQlBMVEX///+0trivsbPx8fKusLK3ubvW19je3+D6+vrZ2tu7vb/q6+vIycvT1NX19fXs7O3GyMnk5ebHycrOz9G7vb7Bw8SPNIu+AAAFPElEQVR4nO2c6dKqMAyGbVkEZBW5/1s9gPqJHEDaJk3QPD+dkek7bbO16ekkCIIgCIIgCIIgCIIgCIIgCILwe5RVcx5oqpJ6KPAE6TXSU6LrOaAeFByXXGmt5vQ/tRfqoUFQFup/dX8qVZxQD9CRMl+YvfeZbA+tsQg35T1ExtTDtKbZWJ9vEqOKeqh25Hsm8E7YUg/WgjLaN4GPacwOtxsvJvpGDuYez/tX6JOwoR60Cam5wGNJtBJ4JImNncBe4kGiuMpWYG9SD2FuEmMrOoV69HvIXASqjnr4nymcplDplFrAJwL7TfiQyD3/d1ujA8zX6dltjY6TyNtluAtUKqIWsUUKoVBzDm0A9PXcqGWs00BMYT+JfFP+DkSgUldqIWu4xWsTNLWSNQBcxUMhV1tTAwlUimthCmoK2brEAE5hyLPyBrYN2UZuLZhArjkUlDcc4GlqIkCFPFMouG3I1JiCRTQj1GqWAFXIMm4ThaLwxxSytDSg3oKnQkiBGbWYRdyLwS9yajGLQEbeBbWYRUCKpQ+FPMsYFaBCnielkO6CWssKNzCBNbWUFWKwauKZWsoKYKUopoWoE1yWzzPDHwDyF0x9xQCUNaXWsQFIWMP6znDiehNjVEitYpMW4KYCz2rwE4idSK3hA86HF/zvJzrW9jXbE+4/XNcp9fh3cHGxp0zTphkOATjbkHuGtctgWrxYoLaTqHmeGi7S2UjUPAtsK1hIPJbA0yk3lXicPfjEsKvkKFZ0SmWiL+LvB4dO9DR9u4We7DapMyMapOm5qTjVaqq4U48m9Pem3mZXD6LO3u6TlnV4/5bqUhYzWxbRtJl5tp/Sj42yWs3+Mf2Yjsh7vYN63qutu/eGiXRzHvXtXV+QzT+nr5QNGMmiT5gn6cODAyvT187uOy823BD2s69luzqb759LcRt31mtmtM6Kea5brUy3VkTlxQ2nvuC+k6BJ22s3kLdps2BDNgJ2moj1trm/ItNixHbzvvZfBk8+VfB1bWIhgk+BrH+JOw7tdbtXY7AjivUtcV/uoLs9a7XZ+TGvNardbZS9y96eyGDrcZfZtzwG5ybFpt4txGvBV1WYPS3hL4gzGNVdpMrTWSSdXNJ66eWhTby1fNlUmsZIOm+LOI2Lts6UNlU3fsTTqZRTq6+Vste//cSokLe7TPFySQOoydAOL62JlFPo5RID6RQqFeJ7DNop9LATARvU7EA3pzmxQPzKMfUUKuwzVMAeQ1uQexMhO/BsQa1owPZU2IKpkNgZ3kGNa8gt6QimNaXWdgex3aTksEj7ZYpXA2fgKwYQb9jy2IaYGxGyW9sFtBQK5H4sBGh3bM1fXUUCLb8AbN1yA83UcDE0eKaGOr1/gZXoc1mkaI9KMIloBpCMKWAHpStIxpRJzDaAlOeDtRe6g3SUyMdZYBXcONRonuDUavi4Q6zYm1rVFJw0n4+hwXL5XHKnEQyBPGqlD0IMhYyCNiSF5OdqU1DKbbwUYgSmjAJvUWgJmzrUwA8oxLiSIQq9grJKeXkLDH/4/THN90feP5Affn8V4/trbYzCNqzjNT4KsR7DBnzc0g20O/tslmmIdscU7mFEN/DuCTMJTTHvevNwiZhNbDxiU9TGZwZniNiPgQE8OecG/oN1xs+yAAv00NV9Je178vKKTUyXKIaeGmWr3a27sPh8xYbC3mi/75qWrVMvqI2+wvf7H8m5c+t4NVGnO5q3P5Km6CKNTVTHxA9+JmUZYFGW1E8MCYIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIJwLP4BxkVQ6eKpOhgAAAAASUVORK5CYII=";
var xhr;

if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE
	xhr = new ActiveXObject("Microsoft.XMLHTTP");
}

window.onload=function(){
	$("#modalAjax").modal("show");
	getRoles();
}

/* ----------------------------------- getRoles ------------------------------------------- */
function getRoles(){
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	for(i in objResponse.records){
		    		var cadena='<option value="'+objResponse.records[i].id+'">'+objResponse.records[i].nombre+'</option>';
		    		document.getElementById("id_roles").innerHTML+=cadena;
		    	}
		    }
		    getComunidades();  
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/roles/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/*-------------------------------------------- getComunidades -----------------------------------------*/
function getComunidades(){
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	for(i in objResponse.records){
		    		var cadena=`<option value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
		    		document.getElementById("id_comunidades").innerHTML+=cadena;
		    	}
		    }
		    $("#modalAjax").modal("hide");
		    var endTime= new Date();
		    document.querySelector("#cardUsuarios div.card-footer span").innerHTML= endTime-initTime;
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/comunidades/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}


/* -------------------------------- #avatar onchange ----------------------------------------------------- */
document.getElementById("avatar").onchange=function () {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            document.querySelector("#cardUsuarios img").src= e.target.result;
        };
        reader.readAsDataURL(this.files[0]);
    }
}

/* ---------------------------------- #id_comunidades onchange ------------------------------------------ */

document.getElementById('id_comunidades').onchange=function(){
	
	document.getElementById("id_provincias").innerHTML='<option value="">-- Elija una opción --</option>';
	objSpinner=document.querySelector("label[for=id_provincias] span.spinner-border");
	objSpinner.className=objSpinner.className.replace(/d-none/g,'');
	document.getElementById("id_municipios").innerHTML='<option value="">-- Elija una opción --</option>';
	if(this.value!=''){
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 &&  xhr.status == 200 ) {
				var objResponse=JSON.parse(xhr.responseText);
			    if(objResponse.status==true){
			    	for(i in objResponse.records){
			    		var cadena=`<option  value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
			    		document.getElementById("id_provincias").innerHTML+=cadena;
			    	}
			    }
			    objSpinner=document.querySelector("label[for=id_provincias] span.spinner-border");
				objSpinner.className+=' d-none';
			}
		}
		xhr.open('GET','http://app.cifo.local/api/private/cifo/provincias/comunidad/'+this.value+'/',true);
		xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
		xhr.send(null);
	}else{
		objSpinner.className+=' d-none';
	}
	
}


/* --------------------------------- #id_provincias onchange ----------------------------------------------*/
document.getElementById('id_provincias').onchange=function(){
	
	document.getElementById("id_municipios").innerHTML='<option value="">-- Elija una opción --</option>';
	objSpinner=document.querySelector("label[for=id_municipios] span.spinner-border");
	objSpinner.className=objSpinner.className.replace(/d-none/g,'');
	if(this.value!=''){
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 &&  xhr.status == 200 ) {
				var objResponse=JSON.parse(xhr.responseText);
			    if(objResponse.status==true){
			    	for(i in objResponse.records){
			    		var cadena=`<option  value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
			    		document.getElementById("id_municipios").innerHTML+=cadena;
			    	}
			    }
			    objSpinner=document.querySelector("label[for=id_municipios] span.spinner-border");
				objSpinner.className+=' d-none';
			}
		}
		xhr.open('GET','http://app.cifo.local/api/private/cifo/municipios/provincia/'+this.value+'/',true);
		xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
		xhr.send(null);
	}else{
		objSpinner.className+=' d-none';
	}
	
}

/* -------------------------------------submit ---------------------------------*/
document.getElementById("btnSave").onclick=function(evt){

	evt.preventDefault();

	// var totOk control de enviament
	var totOk=true;
	
	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
	this.form.querySelectorAll("*").forEach(function(item){
		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
	});
	
	// ------------------------ nombre --------------------
	this.form.nombre.value= this.form.nombre.value.trim();
	var pattern = /\d/;
	if( pattern.test(this.form.nombre.value) || this.form.nombre.value==""){
		this.form.nombre.className+=' is-invalid';
		totOk=false;
	}
	
	// ------------------------ apellidos --------------------
	this.form.apellidos.value= this.form.apellidos.value.trim();
	var pattern = /\d/;
	if( pattern.test(this.form.apellidos.value) || this.form.apellidos.value==""){
		this.form.apellidos.className+=' is-invalid';
		totOk=false;
	}
	
	// ------------------------- Email -----------------------------------
	this.form.email.value = this.form.email.value.trim();
	var emailPattern=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; // Format @
	if( !emailPattern.test(this.form.email.value) || this.form.email.value=="" ){
		// ------------- Afegim la class 'is-invalid' a l'element
		this.form.email.className+=" is-invalid ";
		totOk=false;
	}
	
	// ------------------------- id_roles -----------------------------------------
	if(this.form.id_roles.value==''){
		this.form.id_roles.className+=" is-invalid ";
		totOk=false;
	}
	// ------------------------- password --------------------------------------------
	if(this.form.password.value=='' ){
		this.form.password.className+=" is-invalid ";
		totOk=false;
	}
	// ------------------------- password --------------------------------------------
	if( this.form.password.value!=this.form.querySelector('#password2').value ){
		this.form.querySelector('#password2').className+=" is-invalid ";
		totOk=false;
	}
	
	// ------------------------- active --------------------------------------------
	if(this.form.active.value==''){
		this.form.querySelectorAll('input[name=active]').forEach(function(objInput){
			objInput.className+=" is-invalid ";
		});
		totOk=false;
	}
	
	// ------------------------- enabled --------------------------------------------
	if(this.form.enabled.value==''){
		this.form.querySelectorAll('input[name=enabled]').forEach(function(objInput){
			objInput.className+=" is-invalid ";
		});
		totOk=false;
	}
	// ----------------------- id_comunidades ----------------------------------------
	if(this.form.querySelector('#id_comunidades').value==''){
		this.form.querySelector('#id_comunidades').className+=" is-invalid ";
		totOk=false;
	}
	// ----------------------- id_provincias ----------------------------------------
	if(this.form.querySelector('#id_provincias').value==''){
		this.form.querySelector('#id_provincias').className+=" is-invalid ";
		totOk=false;
	}
	
	// ----------------------- id_municipios ----------------------------------------
	if(this.form.id_municipios.value==''){
		this.form.id_municipios.className+=" is-invalid ";
		totOk=false;
	}
	
	if(totOk){
		$("#modalAjax").modal("show");
		xhr.onreadystatechange =function(){
	        if (xhr.readyState == 4 && xhr.status == 200) {
	          $("#modalAjax").modal("hide");
	          var response=JSON.parse(xhr.responseText);
	          if( response.status){
	        	  document.getElementById("id_municipios").innerHTML='<option value="">-- Elija una opción --</option>';
	        	  document.getElementById("id_provincias").innerHTML='<option value="">-- Elija una opción --</option>';
	        	  document.getElementById("btnReset").click();
	        	  
	        	  document.querySelector("#cardUsuarios img").src=imgSrc ;
	        	  $("#saveModal").modal("show");
	          }else{
	        	  alert(response.msg);
	          }
	        }
		}
		var formData = new FormData(this.form);
      	var object = {};
      	formData.forEach(function(value, key){
      			object[key] = value;  	
      	});
      	object['foto']=this.form.querySelector('img').src;
      	xhr.open('POST','http://app.cifo.local/api/private/cifo/usuarios/',true);
      	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
        xhr.send(JSON.stringify(object));
	}
}

