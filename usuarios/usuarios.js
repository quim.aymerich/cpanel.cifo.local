var xhr;
var tabla;

var glogal_id_provincias;
var global_id_municipios;

if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE
	xhr = new ActiveXObject("Microsoft.XMLHTTP");
}

window.onload=function(){
	tabla=$('#dataTableUsuarios').DataTable({
		"language": {
	        url: '/vendor/datatables/es_ES.json'
	    },
	    "pageLength"	: 5 ,
	    "lengthMenu"	: [ 5, 10, 25, 50, 75, 100 ],
	    "stateSave" 	: true,
	    "columnDefs" 	: [
	    	{"data":"id"		, "targets":0, "searchable": true,  "orderable": true },
	    	{"data":"foto"		, "targets":1, "searchable": false, "orderable": false},
	    	{"data":"nombre"	, "targets":2, "searchable": true,  "orderable": true },
	    	{"data":"apellidos"	, "targets":3, "searchable": true,  "orderable": true },
	    	{"data":"email"		, "targets":4, "searchable": true,  "orderable": true },
	    	{"data":"rol"		, "targets":5, "searchable": true,  "orderable": true },
	    	{"data":"edit"		, "targets":6, "searchable": false, "orderable": false , "className": "text-center"},
	    	{"data":"remove"	, "targets":7, "searchable": false, "orderable": false , "className": "text-center"}
	    ]
	});
	$("#modalAjax").modal("show");
	getRoles();
}
/* ----------------------------------- getRoles ------------------------------------------- */
function getRoles(){
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	for(i in objResponse.records){
		    		var cadena='<option value="'+objResponse.records[i].id+'">'+objResponse.records[i].nombre+'</option>';
		    		document.getElementById("searchRol").innerHTML+=cadena;
		    		document.getElementById("id_roles").innerHTML+=cadena;
		    	}
		    }
		    getComunidades();  
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/roles/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/*-------------------------------------------- getComunidades -----------------------------------------*/
function getComunidades(){
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	for(i in objResponse.records){
		    		var cadena=`<option value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
		    		document.getElementById("id_comunidades").innerHTML+=cadena;
		    	}
		    }
		    getUsuarios();
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/comunidades/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}
/* ---------------------------------------- getUsuarios ------------------------------------------------*/
function getUsuarios(){
	tabla.clear().draw(false);

	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	for(i in objResponse.records){
		    		tabla.row.add(  {
		    		"id" 		: objResponse.records[i].id,
		    		"foto" 		: '<img style="height:40px;" class="rounded-circle" src="'+objResponse.records[i].foto+'" alt="'+objResponse.records[i].email+'">',
		    		"nombre" 	: objResponse.records[i].nombre,
		    		"apellidos" : objResponse.records[i].apellidos,
		    		"email" 	: objResponse.records[i].email,
		    		"rol" 		: objResponse.records[i].rol,
		    		"edit" 		: '<a class="text-info" href="#" onclick="editUsuario('+objResponse.records[i].id+')" aria-label="Editar"><i class="fa fa-edit"></i></a>',
		    		"remove" 	:'<a class="text-danger" href="#" onclick="removeUsuario('+objResponse.records[i].id+')" aria-label="Eliminar"><i class="fa fa-trash"></i></a>',
		    		});
		    	}
		    	tabla.draw(false);
		    }else{
		    	alert(objResponse.msg);
		    }
		    $("#modalAjax").modal("hide");
		    var endTime= new Date();
		    document.querySelector("#cardUsuarios div.card-footer span").innerHTML= endTime-initTime;
		    
		}else if(xhr.readyState == 4 &&  xhr.status == 401){
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==false){
		    	alert(objResponse.msg);
		    	location.href="/login.html";
		    }
		}
	}
	if(document.getElementById("searchRol").value==""){
		// Definim els parametres de sol·licitus HTTP
		xhr.open('GET','http://app.cifo.local/api/private/cifo/usuarios/',true);
	}else{
		var id_roles=document.getElementById("searchRol").value;
		xhr.open('GET','http://app.cifo.local/api/private/cifo/usuarios/rol/'+id_roles,true);
	}
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/* ---------------------------------- removeUsuario ------------------------------------------*/
function removeUsuario(id){
	document.querySelector('#removeModal input').value=id;
	$("#removeModal").modal("show");
}

document.getElementById('btnRemove').onclick=function(){
	var id=document.querySelector('#removeModal input').value;
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			// tot va bé, s'ha rebut la resposta
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	$("#removeModal").modal("hide");
	        	getUsuarios();
		    }
		}
	}
	xhr.open('DELETE','http://app.cifo.local/api/private/cifo/usuarios/'+id,true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
};


/* ----------------------------------- editUsuario --------------------------------------------*/
function editUsuario(id){
	$("#modalAjax").modal("show");
	document.querySelectorAll("#editModal form input[type=text], #editModal form input[type=password] ").forEach(function(item){
		item.value=''
	});
	
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			// tot va bé, s'ha rebut la resposta
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	var objForm= document.querySelector("#editModal form");
		    	for (key in objResponse.record ){
		    		if (typeof objForm[key] !== 'undefined') {
		    			objForm[key].value=objResponse.record[key];
		    		}
		    	}
		    	document.querySelector("#editModal img").src=objResponse.record.foto;
		    	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
		    	objForm.querySelectorAll("*").forEach(function(item){
		    		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
		    	});
		    	global_id_municipios= objResponse.record.id_municipios;
		    	global_id_provincias= objResponse.record.id_provincias;
		    	getProvincias();
		    }
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/cifo/usuarios/'+id,true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/* ------------------------------------------- getProvincias -------------------------------------------*/
function getProvincias(){
	var value= document.getElementById("id_comunidades").value;
	document.getElementById("id_provincias").innerHTML='<option value="">-- Elija una opción --</option>';
	document.getElementById("id_municipios").innerHTML='<option value="">-- Elija una opción --</option>';
	if(value!=''){
		xhr.onreadystatechange = function(e,value){
			if (xhr.readyState == 4 &&  xhr.status == 200 ) {
				var objResponse=JSON.parse(this.responseText);
			    if(objResponse.status==true){
			    	for(i in objResponse.records){
			    		if( objResponse.records[i].id==global_id_provincias ){
			    			var selected='selected="selected"';
			    		}else{
			    			var selected = '';
			    		}
			    		var cadena=`<option ${selected} value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
			    		document.getElementById("id_provincias").innerHTML+=cadena;
			    	}
			    }
			    getMunicipios();
			}
		}
		xhr.open('GET','http://app.cifo.local/api/private/cifo/provincias/comunidad/'+value+'/',true);
		xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
		xhr.send(null);
	}else{
		$("#modalAjax").modal("hide");
		$("#editModal").modal("show");
	}
}

/* -------------------------------------- getMunicipios ----------------------------------------------------------*/
function getMunicipios(){
	var value= document.getElementById("id_provincias").value;
	document.getElementById("id_municipios").innerHTML='<option value="">-- Elija una opción --</option>';
	if(value!=''){
		xhr.onreadystatechange = function(e,value){
			if (xhr.readyState == 4 &&  xhr.status == 200 ) {
				var objResponse=JSON.parse(xhr.responseText);
			    if(objResponse.status==true){
			    	for(i in objResponse.records){
			    		if( objResponse.records[i].id==global_id_municipios ){
			    			var selected='selected="selected"';
			    			console.log('selected');
			    		}else{
			    			var selected = '';
			    		}
			    		var cadena=`<option ${selected}  value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
			    		document.getElementById("id_municipios").innerHTML+=cadena;
			    	}
			    }
			    $("#modalAjax").modal("hide");
			    $("#editModal").modal("show");
			}
		}
		xhr.open('GET','http://app.cifo.local/api/private/cifo/municipios/provincia/'+value+'/',true);
		xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
		xhr.send(null);
	}else{
		$("#modalAjax").modal("hide");
		$("#editModal").modal("show");
	}
}


/* ---------------------------------- #id_comunidades onchange ------------------------------------------ */

document.getElementById('id_comunidades').onchange=function(){
	
	document.getElementById("id_provincias").innerHTML='<option value="">-- Elija una opción --</option>';
	objSpinner=document.querySelector("label[for=id_provincias] span.spinner-border");
	objSpinner.className=objSpinner.className.replace(/d-none/g,'');
	document.getElementById("id_municipios").innerHTML='<option value="">-- Elija una opción --</option>';
	if(this.value!=''){
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 &&  xhr.status == 200 ) {
				var objResponse=JSON.parse(xhr.responseText);
			    if(objResponse.status==true){
			    	for(i in objResponse.records){
			    		var cadena=`<option  value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
			    		document.getElementById("id_provincias").innerHTML+=cadena;
			    	}
			    }
			    objSpinner=document.querySelector("label[for=id_provincias] span.spinner-border");
				objSpinner.className+=' d-none';
			}
		}
		xhr.open('GET','http://app.cifo.local/api/private/cifo/provincias/comunidad/'+this.value+'/',true);
		xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
		xhr.send(null);
	}else{
		objSpinner.className+=' d-none';
	}
	
}

/* --------------------------------- #id_provincias onchange ----------------------------------------------*/
document.getElementById('id_provincias').onchange=function(){
	
	document.getElementById("id_municipios").innerHTML='<option value="">-- Elija una opción --</option>';
	objSpinner=document.querySelector("label[for=id_municipios] span.spinner-border");
	objSpinner.className=objSpinner.className.replace(/d-none/g,'');
	if(this.value!=''){
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 &&  xhr.status == 200 ) {
				var objResponse=JSON.parse(xhr.responseText);
			    if(objResponse.status==true){
			    	for(i in objResponse.records){
			    		var cadena=`<option  value="${objResponse.records[i].id}">${objResponse.records[i].nombre}</option>`;
			    		document.getElementById("id_municipios").innerHTML+=cadena;
			    	}
			    }
			    objSpinner=document.querySelector("label[for=id_municipios] span.spinner-border");
				objSpinner.className+=' d-none';
			}
		}
		xhr.open('GET','http://app.cifo.local/api/private/cifo/municipios/provincia/'+this.value+'/',true);
		xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
		xhr.send(null);
	}else{
		objSpinner.className+=' d-none';
	}
	
}

/* --------------------------- #searchRol onchange -------------------------------------------- */
document.getElementById("searchRol").onchange=function(){
	initTime= new Date();
	$("#modalAjax").modal("show");
	getUsuarios();
};

/* ---------------------------- #avatar onchange ----------------------------------------------- */
document.getElementById("avatar").onchange=function () {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log(e.target.result);
            document.querySelector("#editModal img").src= e.target.result;
        };
        reader.readAsDataURL(this.files[0]);
    }
}

/* ----------------------- #btnSave onclick -----------------------------------------------------*/
document.getElementById("btnSave").onclick=function(evt){

	var objForm= document.querySelector("#editModal form");

	// var totOk control de enviament
	var totOk=true;
	
	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
	objForm.querySelectorAll("*").forEach(function(item){
		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
	});
	
	// ------------------------ nombre --------------------
	objForm["nombre"].value= objForm["nombre"].value.trim();
	var pattern = /\d/;
	if( pattern.test(objForm["nombre"].value) || objForm["nombre"].value==""){
		objForm["nombre"].className+=' is-invalid';
		totOk=false;
	}
	
	// ------------------------ apellidos --------------------
	objForm["apellidos"].value= objForm["apellidos"].value.trim();
	var pattern = /\d/;
	if( pattern.test(objForm["apellidos"].value) || objForm["apellidos"].value==""){
		objForm["apellidos"].className+=' is-invalid';
		totOk=false;
	}
	
	// ------------------------- Email -----------------------------------
	objForm["email"].value= objForm["email"].value.trim();
	var emailPattern=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; // Format @
	if( !emailPattern.test(objForm["email"].value) || objForm["email"].value=="" ){
		// ------------- Afegim la class 'is-invalid' a l'element
		objForm["email"].className+=" is-invalid ";
		totOk=false;
	}
	
	if(totOk){
		
		xhr.onreadystatechange =function(){
	        if (xhr.readyState == 4 && xhr.status == 200) {
	          var response=JSON.parse(xhr.responseText);
	          if( response.status){
	        	  $("#editModal").modal("hide");
	        	  getUsuarios();
	          }else{
	        	  alert(response.msg);
	          }
	        }
		}
		
		if(document.querySelector('#avatar').value!=''){
			var file = document.querySelector('#avatar').files[0];
			getBase64(file).then( data => {
				var formData = new FormData(document.querySelector("#editModal form"));
		      	var object = {};
		      	formData.forEach(function(value, key){
		      		if(key=='passwd' ){
		      			if(value!='' ){
		      				object['password'] = value;
		      			}
		      		}else{
		      			object[key] = value;  
		      		}	
		      	});
		      	object['foto']=data;
		      	xhr.open('PUT','http://app.cifo.local/api/private/cifo/usuarios/',true);
		      	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
		        xhr.send(JSON.stringify(object));
			});
		}else{
			var formData = new FormData(document.querySelector("#editModal form"));
	      	var object = {};
	      	formData.forEach(function(value, key){
	      		if(key=='passwd' ){
	      			if(value!='' ){
	      				object['password'] = value;
	      			}
	      		}else{
	      			object[key] = value;  
	      		}
	      	});
	      	xhr.open('PUT','http://app.cifo.local/api/private/cifo/usuarios/',true);
	      	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	        xhr.send(JSON.stringify(object));
		}
	}
}


function getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }