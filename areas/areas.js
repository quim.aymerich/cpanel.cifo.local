var xhr;
var tabla;
$("#modalAjax").modal("show");


if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE
	xhr = new ActiveXObject("Microsoft.XMLHTTP");
}

window.onload=function(){
	
	tabla=$('#dataTableAreas').DataTable({
		"language": {
	        url: '/vendor/datatables/es_ES.json'
	    },
	    "pageLength"	: 5 ,
	    "lengthMenu"	: [ 5, 10, 25, 50, 75, 100 ],
	    "stateSave" 	: true,
	    "columnDefs" 	: [
	    	{"data":"id"		, "targets":0, "searchable": true,  "orderable": true },
	    	{"data":"nombre"	, "targets":1, "searchable": true,  "orderable": true },
	    	{"data":"active"	, "targets":2, "searchable": true,  "orderable": true },
	    	{"data":"edit"		, "targets":3, "searchable": false, "orderable": false , "className": "text-center"},
	    	{"data":"remove"	, "targets":4, "searchable": false, "orderable": false , "className": "text-center"}
	    ]
	});
	
	$('#descripcion').summernote({
		  height		: 200,            // set editor height
		  minHeight		: null,           // set minimum height of editor
		  maxHeight		: null,           // set maximum height of editor
		  lang			: 'es-ES'		  // default: 'en-US'
	});
	getAreas();
}

/* ---------------------------------------- getAreas ------------------------------------------------*/
function getAreas(){
	tabla.clear().draw(false);

	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	for(i in objResponse.records){
		    		if(objResponse.records[i].active=="1"){
		    			var circle='<span class="text-success"><i class="fas fa-check-circle"></i></span>';
		    		}else{
		    			var circle='<span class="text-danger"><i class="fas fa-times-circle"></i></span>';
		    		}
		    		tabla.row.add(  {
		    		"id" 		: objResponse.records[i].id,
		    		"nombre" 	: objResponse.records[i].nombre,
		    		"active" 	: circle,
		    		"edit" 		: '<a class="text-info" href="#" onclick="editArea('+objResponse.records[i].id+')" aria-label="Editar"><i class="fa fa-edit"></i></a>',
		    		"remove" 	: '<a class="text-danger" href="#" onclick="removeArea('+objResponse.records[i].id+')" aria-label="Eliminar"><i class="fa fa-trash"></i></a>',
		    		});
		    	}
		    	tabla.draw(false);
		    }else{
		    	alert(objResponse.msg);
		    }
		    $("#modalAjax").modal("hide");
		    var endTime= new Date();
		    document.querySelector("#cardAreas div.card-footer span").innerHTML= endTime-initTime;
		    
		}else if(xhr.readyState == 4 &&  xhr.status == 401){
			location.href="/login.html";
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/formacion/areas/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/* ---------------------------------- removeArea ------------------------------------------*/
function removeArea(id){
	document.querySelector('#removeModal input').value=id;
	$("#removeModal").modal("show");
}

document.getElementById('btnRemove').onclick=function(){
	var id=document.querySelector('#removeModal input').value;
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			// tot va bé, s'ha rebut la resposta
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	$("#removeModal").modal("hide");
		    	getAreas();
		    }else{
		    	alert(objResponse.msg);
		    }
		}
	}
	xhr.open('DELETE','http://app.cifo.local/api/private/formacion/areas/'+id,true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
};

/* ----------------------------------- editArea --------------------------------------------*/
function editArea(id){
	
	$("#modalAjax").modal("show");
	
	document.querySelectorAll("#editModal form input[type=text]").forEach(function(item){
		item.value=''
	});
	document.querySelectorAll("#editModal form input[type=radio]").forEach(function(item){
		item.checked=false;
	});
	
	
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 &&  xhr.status == 200 ) {
			// tot va bé, s'ha rebut la resposta
			var objResponse=JSON.parse(xhr.responseText);
		    if(objResponse.status==true){
		    	var objForm= document.querySelector("#editModal form");
		    	for (key in objResponse.record ){
		    		if(key=='descripcion'){
	    				$('#descripcion').summernote('code',objResponse.record['descripcion']);
	    			}else if (typeof objForm[key] !== 'undefined') {
		    			objForm[key].value=objResponse.record[key];
		    		}
		    	}
		    	
		    	
		    	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
		    	objForm.querySelectorAll("*").forEach(function(item){
		    		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
		    	});
		    	$("#modalAjax").modal("hide");
		    	$("#editModal").modal("show");
		    }else{
		    	$("#modalAjax").modal("hide");
		    	alert(objResponse.msg);
		    }
		    
		}
	}
	xhr.open('GET','http://app.cifo.local/api/private/formacion/areas/'+id+'/',true);
	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
	xhr.send(null);
}

/* ----------------------- #btnSave onclick -----------------------------------------------------*/
document.getElementById("btnSave").onclick=function(evt){

	var objForm= document.querySelector("#editModal form");

	// var totOk control de enviament
	var totOk=true;
	
	// ---- Netejar classes 'is-invalid' i 'is-valid' ----
	objForm.querySelectorAll("*").forEach(function(item){
		item.className=item.className.replace(/(is-valid|is-invalid)/g,'');
	});
	
	// ------------------------ nombre --------------------
	objForm["nombre"].value= objForm["nombre"].value.trim();
	var pattern = /\d/;
	if( pattern.test(objForm["nombre"].value) || objForm["nombre"].value==""){
		objForm["nombre"].className+=' is-invalid';
		totOk=false;
	}
	
	
	
	// ------------------------- active --------------------------------------------
	if(objForm["active"].value==''){
		objForm.querySelectorAll('input[name=active]').forEach(function(objInput){
			objInput.className+=" is-invalid ";
		});
		totOk=false;
	}
	
	if(totOk){
		
		xhr.onreadystatechange =function(){
	        if (xhr.readyState == 4 && xhr.status == 200) {
	          var response=JSON.parse(xhr.responseText);
	          if( response.status){
	        	  $("#editModal").modal("hide");
	        	  getAreas();
	          }else{
	        	  alert(response.msg);
	          }
	        }
		}
		
		var formData = new FormData(document.querySelector("#editModal form"));
      	var object = {};
      	formData.forEach(function(value, key){
      		if(key=='files'){
      			object['descripcion']= $('#descripcion').summernote('code');
      		}else{
      		    object[key] = value;  
      		}
      	});
      	
      	xhr.open('PUT','http://app.cifo.local/api/private/formacion/areas/',true);
      	xhr.setRequestHeader("authorization", window.sessionStorage.getItem("Authorization"));
        xhr.send(JSON.stringify(object));
		
	}
}
